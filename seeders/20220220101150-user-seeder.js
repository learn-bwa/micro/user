'use strict';

const bcrypt = require('bcrypt')
module.exports = {
  async up (queryInterface, Sequelize) {
  
    await queryInterface.bulkInsert('users', [
      {
        name:'angga saputra',
        profession: 'backend engineer',
        role: 'admin',
        email: 'angga@angga.com',
        password: await bcrypt.hash('secret123', 10),
        created_at: new Date(),
        updated_at: new Date(),
      },
      {
        name:'evie destriani',
        profession: 'front engineer',
        role: 'student',
        email: 'evie@evie.com',
        password: await bcrypt.hash('secret123', 10),
        created_at: new Date(),
        updated_at: new Date(),
      },
    ], {});
  
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('users', null, {});
  }
};
