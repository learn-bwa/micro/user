'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    queryInterface.renameColumn('refresh_tokens','create_at', 'created_at');
  },

  async down (queryInterface, Sequelize) {
    queryInterface.renameColumn('refresh_tokens','created_at', 'create_at');
  }
};
