module.exports = (sequelize, Datatypes) => {
    const RefreshToken = sequelize.define('RefreshToken', {
        id: {
            type: Datatypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false,
        },
        token: {
            type: Datatypes.TEXT,
            allowNull: false,
        },
        userId: {
            field: 'user_id',
            type: Datatypes.INTEGER,
            allowNull: false,
        },
        createdAt: {
            field: 'created_at',
            type: Datatypes.DATE,
            allowNull: false,
        },
        updatedAt: {
            type: Datatypes.DATE,
            field: 'updated_at',
            allowNull: false,
        }
    }, {
        tableName: 'refresh_tokens',
        timestamps: true
    });

    return RefreshToken
}