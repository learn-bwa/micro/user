const { User } = require('../../../models');

module.exports = async (req, res, next) => {
    const modelOptions = {
        attributes: ['id', 'name', 'email', 'role', 'profession', 'avatar']
    }

    const userIds = req.query.user_ids || [];
    if (userIds.length > 0) {
        modelOptions.where = {
            id: userIds
        }
    }
    
    const users = await User.findAll(modelOptions)

    return res.json({
        status: 'success',
        data: users
    })
}